<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hieu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' GY.Vx:t`<?f1v-b=O w{BB14CsqO3>8?`EUWFr,R0(j26mOKfc,g;1eV0?0N}>i' );
define( 'SECURE_AUTH_KEY',  'ng%(8V&O9nby%~Y=Zxyj7+AdFRr[&_uSRm`oS:[DkWYIkD1Mk3r]n=wN!A>fo2_#' );
define( 'LOGGED_IN_KEY',    'SS!Utl=yhnM)G`VRv|z8;L#{PM&]t>~UL}8|I@oM7aLgmBtKq+R}kHrFnE^%~(8)' );
define( 'NONCE_KEY',        'UyaeE<0&H;O[V:dXtwS9lhAH_voo/`t4mXN![>|61{3<BIV}m*%Pk1wuS@#OqS=n' );
define( 'AUTH_SALT',        '<lJ_F`I[tOa5RAy|GFX.XVPbodL_3.0dhF+I>lbK^;{Dxh5uH.{JjDN1v<v,43>J' );
define( 'SECURE_AUTH_SALT', 'yFTdZN%yMdZG1CcV?67w1=bY?P(Il ~ES20LhC.H0P]6q@)}iOB+;=jtMDC0(e/s' );
define( 'LOGGED_IN_SALT',   'tX.r6yh%:p<E;w~#DUxlBby^RTHvZ_QKj3CG3`^Yb aW^+:~[c@(E~J:z8 ^c7&p' );
define( 'NONCE_SALT',       'LDB/Y(C@](b/oc>J2;2LXoh(KW`cTXro H9bTkRi$^!#t u>*o ;^7MR{{ hVVS`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'hieu_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
